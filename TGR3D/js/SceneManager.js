function SceneManager(canvas) {

    const clock = new THREE.Clock(true);

    const screenDimensions = {
        width: canvas.width,
        height: canvas.height
    }

    const T = THREE
    const axesHelper = new T.AxesHelper(100)
    const ambientLight = new T.AmbientLight({
        name: "",
        color: 0xffffff,
        intensity: 1.0
    });
    let directionalLight = new T.DirectionalLight(0xffffff, 0.1);
    const scene = buildScene();
    const renderer = buildRender(screenDimensions);
    const camera = buildCamera(screenDimensions);
    let orbitControls = new THREE.OrbitControls(camera)
    const sceneSubjects = createSceneSubjects(scene);
        function buildScene() {
        const scene = new THREE.Scene();

    //*****************************************************************************************************
    //                                           AXIS HELPER
    //scene.add(axesHelper)
    //*****************************************************************************************************
    //*****************************************************************************************************
    //                                              SKYBOX
    //*****************************************************************************************************
        let skybox;

        function lights() {



            directionalLight.position.set(0, 47, 200);

            directionalLight.castShadow = true
            directionalLight.shadow.mapSize.width = 1024;
            directionalLight.shadow.mapSize.height = 1024;
            directionalLight.shadow.camera.near = 1;
            directionalLight.shadow.camera.far = 1000;

            scene.add(ambientLight)
            scene.add(directionalLight)

        }

        function loadSkybox() {

            let urls = [

                "assets/home_skybox/definitiva_wrap_3.jpg",
                "assets/home_skybox/definitiva_red_5.jpg",
                "assets/home_skybox/2 definitiva_yellowblue_2.jpg",
                "assets/home_skybox/definitiva_black_6.jpg",
                "assets/home_skybox/definitiva_bluem_4.jpg",
                "assets/home_skybox/1 definitiva_yellowmagentagreen_1.jpg",

                ];
            
            skybox = new T.CubeTextureLoader().load(urls);
            skybox.encoding = T.sRGBEncoding
            scene.background = skybox;
        }
        //*******************************************************************************************************
        //                                               PARTICLE SYSTEM
        //*******************************************************************************************************
        var particleGeo = new THREE.Geometry();
        var particleMat = new THREE.PointsMaterial({
            color: '#ffffff',
            size: 0.5,
            map: new THREE.TextureLoader().load("assets/particle/particle.jpg"),
            transparent: true,
            blending: THREE.AdditiveBlending,
            depthWrite: false
        });

        var particleCount = 19000;
        var particleDistance = 590;

        for (var i = 0; i < particleCount; i++) {
            var posX = (Math.random() - 0.5) * particleDistance;
            var posY = (Math.random() - 0.5) * particleDistance;
            var posZ = (Math.random() - 0.5) * particleDistance;
            var particle = new THREE.Vector3(posX, posY, posZ);

            particleGeo.vertices.push(particle);
        }

        var particleSystem = new THREE.Points(
            particleGeo,
            particleMat
        );
        particleSystem.name = 'particleSystem';

        scene.add(particleSystem);
    //*******************************************************************************************************
    //                                              GLTF MODELS
    //*******************************************************************************************************
        function loadObject(name) {

            const loader = new T.GLTFLoader()

            loader.load(name, gltf => {

                scene.add(gltf.scene)
                console.log("models loaded")
            })
        }
        lights()
        loadSkybox()
        loadObject("assets/gltf/nord/nord.gltf")
        loadObject("assets/gltf/tet/tet.gltf")
        window.addEventListener("click", onClick, false);

        function onClick() {
        alert("Replace me with code to add an object!");
        			}
        return scene;
    }

    //*******************************************************************************************************
    //                                               SCENE SUBJECTS
    //*******************************************************************************************************
    function createSceneSubjects(scene) {
        const sceneSubjects = [
            new GeneralLights(scene),
            new SceneSubject(scene)
        ];

        return sceneSubjects;
    }

    this.update = function () {
        const elapsedTime = clock.getElapsedTime();

  //*******************************************************************************************************
  //                                               RAYCASTER
  //*******************************************************************************************************

       //PROJECTOR
        var projector = new THREE.Projector();
        function onDocumentMouseDown (event) {

            var vector = new THREE.Vector3 ((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * + 1, 0.5);
            vector = vector.unproject (camera);

            var raycaster =  new THREE.Raycaster (camera.position,vector.sub(camera.position).normalize());
            var intersects = raycaster.intersectObjects(["assets/gltf/nord/nord.gltf", "assets/gltf/nord/nord.gltf"]);

            if (intersects.length > 0) {
                console.log (intersects[0]);
                intersects [0].object.material.transparent = true;
                intersects [0].object.material.opacity = 0.1;
            }

        }

        //LIGHTS ON MODELS
        for (let i = 0; i < sceneSubjects.length; i++)
            sceneSubjects[i].update(elapsedTime);
        let delta;
        delta = clock.getDelta()

        orbitControls.update(delta);
        renderer.render(scene, camera);
    }

    this.onWindowResize = function () {
        const { width, height } = canvas;

        screenDimensions.width = width;
        screenDimensions.height = height;

        camera.aspect = width / height;
        camera.updateProjectionMatrix();

        renderer.setSize(width, height);

    }


    //*******************************************************************************************************
    //                                               RENDER
    //*******************************************************************************************************
    function buildRender({ width, height }) {
        const renderer = new THREE.WebGLRenderer({ canvas: canvas, antialias: true, alpha: true });
        const DPR = (window.devicePixelRatio) ? window.devicePixelRatio : 1;
        renderer.setPixelRatio(DPR);
        renderer.setSize(width, height);

        renderer.gammaInput = true;
        renderer.gammaOutput = true;

        return renderer;

    }
    //*******************************************************************************************************
    //                                               CAMERA
    //*******************************************************************************************************
    function buildCamera({ width, height }) {
        const aspectRatio = width / height;
        const fieldOfView = 130;
        const nearPlane = 0.3;
        const farPlane = 1000
        const camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);

        camera.position.set(42.26, 20.98, 30.13)
        camera.rotation.set(-5.47, 0.8, 6.72)
        return camera;
    }
}

/*  camera.position.z = 73;
    camera.position.x = 20;
    camera.position.y = 50;
    camera.lookAt(new THREE.Vector3(0, 0, 0)); */
