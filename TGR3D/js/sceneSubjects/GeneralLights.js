function GeneralLights(scene) {
	
	const light = new THREE.PointLight("#e222ff", 1);
    scene.add(light);
	
	this.update = function(time) {
		light.intensity = (Math.sin(time)+1.57)/1.7                ;
		light.color.setHSL( Math.sin(time), 0.5, 0.5 );
	}
}