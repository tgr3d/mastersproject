const canvas = document.getElementById("canvas");

$(document).ready(function () {
	window.onload = function () {
		$(".pre-loader").fadeOut(500, function () { $(".pre-loader").remove(); });
	}
});

var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
if (!isChrome) {
	$('#iframeAudio').remove()
}
else {
	$('#playAudio').remove() 
}


const sceneManager = new SceneManager(canvas);



function bindEventListeners() {
	window.onresize = resizeCanvas;
	resizeCanvas();
}

function resizeCanvas() {
	canvas.style.width = '100%';
	canvas.style.height= '100%';
	canvas.width  = canvas.offsetWidth;
	canvas.height = canvas.offsetHeight;
    sceneManager.onWindowResize();
}

function render() {
    requestAnimationFrame(render);
	sceneManager.update();
}

bindEventListeners();
render();
